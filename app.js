var app = require('express')();
var http = require('http').Server(app);
const port = 9999


var io = require('socket.io')(http);
var redis = require('redis');
var client = redis.createClient();

var send = require('./send.js');
var messages = require('./messages.js');

app.set('socketio', io);
app.set('redisClient',client);
app.use('/send',send);
app.use('/messages',messages);

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.of('/ws').on('connection', function(socket){
    socket.on('chat message', function(msg){
    	client.lrange('messages', 0, -1, function(err, reply) {
    		if (err) throw err;
    		console.log(reply)
    	});
    });
  });
http.listen(port)