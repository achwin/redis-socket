var express = require('express');
var router = express.Router();

router.get('/', function(req, res){
	var client = req.app.get('redisClient');
	client.lrange('messages', 0, -1, function(err, reply) {
		if (err) throw err;
	    res.send(reply);
	});
});

module.exports = router;