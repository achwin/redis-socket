var express = require('express');
var router = express.Router();
var socket = require('socket.io-client')('http://localhost:9999/ws');

router.get('/', function(req, res){
	var client = req.app.get('redisClient');
	client.rpush(['messages', req.query.text], function(err, reply) {
    	if (err) throw err;
    	socket.emit('chat message', req.query.text);
    	res.send(req.query.text);
	});
});

module.exports = router;